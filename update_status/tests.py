# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_message
from .models import Message
from .forms import Message_Form

# Create your tests here.
class UpdateStatusTest(TestCase):
	def test_update_status_is_exist(self):
		response = Client().get('/update_status/')
		self.assertEqual(response.status_code, 200)
		
	def test_update_status_using_index_func(self):
		found = resolve('/update_status/')
		self.assertEqual(found.func, index)
		
	def test_form_validaton_for_blank_items(self):
		form = Message_Form(data={'message': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
		form.errors['message'],
		["This field is required."]
		)
		
'''	def test_form_message_input_has_placeholder_and_css_classes(self):
		form = Message_Form()
		self.assertIn('class="message-form-input', form.as_p())
		self.assertIn('id="id_message"',form.as_p())'''
		
'''	def test_update_status_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/update_status/',{'message':test})
		self.assertEqual(response_post.status_code, 200)
		
		response= Client().get('/update_status/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)
		
	def test_update_status_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/update_status/',{'message': ''})
		self.assertEqual(response_post.status_code, 200)
		
		response= Client().get('/update_status/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)'''