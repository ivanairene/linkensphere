from django import forms

class Message_Form(forms.Form):
    error_messages = {
        'required': 'This field is required',
    }
    attrs = {
        'class': 'form'
    }
    message = forms.CharField(widget=forms.Textarea(attrs=attrs), required=True, label='')
