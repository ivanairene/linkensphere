# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message

# Create your views here.

response = {}
def index(request):    
    message = Message.objects.order_by('-created_date')
    response['message'] = message
    html = 'update_status/update_status.html'
    response['message_form'] = Message_Form
    return render(request, html, response)

def add_message(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['message'] = request.POST['message']
        message = Message(message=response['message'])
        message.save()
        return HttpResponseRedirect('/update_status/')
    else:
        return HttpResponseRedirect('/update_status/')