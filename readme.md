[![pipeline status](https://gitlab.com/ivanairene/linkens-sphere/badges/master/pipeline.svg)](https://gitlab.com/ivanairene/my-first-repo/commits/master)
[![coverage report](https://gitlab.com/ivanairene/linkens-sphere/badges/master/coverage.svg)](https://gitlab.com/ivanairene/my-first-repo/commits/master)

Live at: http://linkens-sphere.herokuapp.com/

Nama-nama kelompok:

- Atalya Yoseba/1606824471
- Ivana Irene Thomas/1606887352
- Kevin Prakarsa/1606917696
- Yogi Lesmana/1606878051


#Panduan untuk mengerjakan Tugas 1 PPW yeyyyyy

1. Clone repository

`git clone https://gitlab.com/ivanairene/linkens-sphere.git`

2. Buat branch fitur kalian sendiri

contoh, jika mengerjakan fitur add_friend, maka buat:

`git checkout -b add_friend`

command tersebut akan membuat kalian keluar dari branch master, dan membuat sebuah branch local baru 'add_friend'

coba lakukan command

`git status`

Jika muncul seperti ini:

`On branch [nama branch]`

maka kalian siap ke langkah selanjutnya :D

3. Buat virtual environment

`python -m venv env`

atau jika venv bukan module di local kalian coba

`python -m virtual env`

4. Install dependencies

ke virtual environment kalian,

`env\Scripts\activate.bat` (jika di windows)
atau
`source env/bin/activate` (jika di linux/mac)

lalu jalankan

`pip install -r requirements.txt`

5. Coba apakah setup berhasil dengan command

`python manage.py runserver`

Sekarang kita siap bekerja! yey!!

Jangan lupa untuk selalu bekerja, dan melakukan push di branch kalian, bukan di master. Lalu saling contact contact ya kalo merasa fitur yang kalian bikin sudah ready untuk di merge ke master.

SEMANGATTT :D


