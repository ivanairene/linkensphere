"""linkensphere URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import view_profile.urls as view_profile
from view_profile.views import index as index_view_profile
import add_friend.urls as add_friend
import dashboard.urls as dashboard
import update_status.urls as update_status

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^dashboard/', include(dashboard,namespace='dashboard')),
    url(r'^view_profile/', include(view_profile, namespace='view_profile')),
    url(r'^add_friend/',include(add_friend,namespace="add_friend")),
	url(r'^update_status/', include(update_status, namespace='update_status')),
    url(r'^$', RedirectView.as_view(url='/update_status/', permanent=True), name='index'),
]
