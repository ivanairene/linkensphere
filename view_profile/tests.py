# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from django.test import Client
from django.http import HttpRequest
from django.urls import resolve
from django.contrib.auth.models import *
from .views import index
from .models import Profile

# Create your tests here.
# class ViewProfileUnitTest(TestCase):
#     def test_view_profile_url_is_exist(self):
#         response = Client().get('/view_profile/')
#         print(response)
#         self.assertEqual(response.status_code, 200)
#
#     def test_view_profile_using_index_func(self):
#         found = resolve('/view_profile/')
#         self.assertEqual(found.func, index)
