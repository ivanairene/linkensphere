# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import *
from .models import Profile

def index(request):
    profile = Profile.objects.get(pk=1)
    split_expertise = profile.expert.split(',') #Sleeping,marketing,Collector [sleeping,marketing,collector]
    response = {'profile':profile, 'split_expertise':split_expertise}
    return render(request, 'view_profile/view_profile.html', response)


# Birthday
# Gender
# Expertise : list
# Description
# Email
