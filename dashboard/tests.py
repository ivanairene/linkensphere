# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.test import TestCase, Client
from django.urls import resolve

from .views import index
from view_profile.views import Profile
from add_friend.models import Friend
from update_status.models import Message

class DashboardTest(TestCase):
    def test_dashboard_url_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 200)
    def test_dashboard_using_index_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, index)
    def test_dashboard_name_exist(self):
        try: 
            profile = Profile.objects.get(pk=1)
            profileName = profile.name
        except: 
            profileName = ""
        response= Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn(profileName, html_response)
    def test_dashboard_friend_count(self):
        try:
            friendCount = str(Friend.objects.count())
        except:
            friendCount = ""
        response=Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn(friendCount, html_response)
    def test_dashboard_feed_count(self):
        try:
            feedCount = str(Message.objects.count())
        except:
            feedCount = ""
        response=Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn(feedCount, html_response)
    def test_dashboard_latest_post_exist(self):
        try:
            latestStatus = Message.objects.latest('created_date')
        except:
            latestStatus = ""
        response=Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn(latestStatus, html_response)

# Create your tests here.
