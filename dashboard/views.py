from django.shortcuts import render
from view_profile.models import Profile
from update_status.models import Message
from add_friend.models import Friend
# Create your views here.

def index(request):
	feedCount = Message.objects.count()
	try:
		latestStatus = Message.objects.latest('created_date')
	except:
		latestStatus = ""
	try:
		profile = Profile.objects.get(pk=1)
	except:
		profile = None
	friendCount = Friend.objects.count()
	response = {'profile': profile, 'feedCount': feedCount, 'friendCount': friendCount, 'latestStatus': latestStatus}
	return render(request, 'dashboard.html', response)