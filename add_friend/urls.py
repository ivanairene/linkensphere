from django.conf.urls import url, include
from .views import index,add_friend

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add', add_friend, name='add_friend'),
]
