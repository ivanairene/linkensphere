# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Friend
from .forms import Friend_Form
# from .forms import
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class AddFriendUnitTest(TestCase):
    def test_add_friend_url_is_exist(self):
        response = Client().get('/add_friend/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend_using_index_func(self):
        found = resolve('/add_friend/')
        self.assertEqual(found.func, index)

    def test_model_can_create_friend(self):
        # Creating a new activity
        new_activity = Friend.objects.create(name='kevin',link="https://kapeganteng.herokuapp.com/")

        # Retrieving all available activity
        counting_all_available_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend, 1)

    def test_form_friend_input_has_placeholder_and_css_classes(self):
        form = Friend_Form()
        self.assertIn('id="id_name"', form.as_p())
        self.assertIn('id="id_link', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Friend_Form(data={'name': '', 'link': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )
        self.assertEqual(
            form.errors['link'],
            ["This field is required."]
        )

    def test_add_friend_post_success_and_render_the_result(self):
        test_name = 'kevin'
        test_link = 'https://kapeganteng.herokuapp.com/'
        response_post = Client().post('/add_friend/add', {'name': test_name, 'link': test_link})
        self.assertEqual(response_post.status_code, 302)
        response= Client().get('/add_friend/')
        html_response = response.content.decode('utf8')
        self.assertIn(test_name, html_response)
        self.assertIn(test_link, html_response)

    def test_add_friend_post_error_and_render_the_result(self):
        test = 'testError'
        response_post = Client().post('/add_friend/add', {'name': '','link': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/add_friend/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

# class AddFriendFunctionalTest(TestCase):

#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(AddFriendFunctionalTest, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(AddFriendFunctionalTest, self).tearDown()

#     def test_input_Friend(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://127.0.0.1:8000/add_friend/')
#         # find the form element
#         name = selenium.find_element_by_id('id_name')
#         link = selenium.find_element_by_id('id_link')

#         submit = selenium.find_element_by_id('submit')

#         # Fill the form with data
#         name.send_keys('Kevin Prakasa')
#         link.send_keys('https://kapeganteng.herokuapp.com/')

#         # submitting the form
#         submit.send_keys(Keys.RETURN)
