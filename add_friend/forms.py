from django import forms

class Friend_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    title_attrs = {
        'type': 'text',
        'placeholder':'Enter your friend\'s name...'
    }

    link_attrs = {
        'type': 'url',
        'placeholder':'Enter your friend\'s URL...'
    }

    name = forms.CharField(label='Name', required=True, max_length=27, widget=forms.TextInput(attrs=title_attrs))
    link = forms.URLField(label='URL', required=True, widget=forms.URLInput(attrs=link_attrs))
