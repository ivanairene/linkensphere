# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend
from django.shortcuts import render

# Create your views here.
response={}
def index(request):
    friends = Friend.objects.all()
    response['friends'] = friends
    html = 'add_friend.html'
    response['friend_form'] = Friend_Form
    return render(request, html, response)

def add_friend(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['link'] = request.POST['link']
        friend = Friend(name=response['name'],link=response['link'])
        friend.save()
        return HttpResponseRedirect('/add_friend/')
    else:
        return HttpResponseRedirect('/add_friend/')
